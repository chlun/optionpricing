#include "Curve.h"
#include "Maths/Interpolate.h"

Curve::Curve(double leftBoundary, double rightBoundary, int numPoints)
	: mGrid(leftBoundary, rightBoundary, numPoints), mValues(numPoints)
{
}

Curve::Curve(double leftBoundary, double rightBoundary, Vector values)
	: mGrid(leftBoundary, rightBoundary, values.size()), mValues(values)
{
}

void Curve::setValues(const Vector& values)
{
	mValues = values;
}

void Curve::setValueAt(int idx, double value)
{
	mValues[idx] = value;
}

Vector& Curve::getValues()
{
	return mValues;
}

std::vector<double> Curve::getDomain() const
{
	std::vector<double> domain;
	for (auto i = 0; i < mGrid.N(); ++i)
	{
		domain.push_back(mGrid.x(i));
	}

	return domain;
}

std::vector<std::pair<double, double>> Curve::getPointsInCurve() const
{
	std::vector<std::pair<double, double>> points;
	for (auto i = 0; i < mValues.size(); ++i)
	{
		points.push_back(std::make_pair(mGrid.x(i), mValues[i]));
	}

	return points;
}

double Curve::operator()(double x) const
{
	auto idx = mGrid.toIndex(x);
	auto leftVal = mValues[idx];
	auto rightVal = idx == mValues.size() - 1 ? leftVal : mValues[idx + 1];

	return linearInterpolate(leftVal, rightVal, mGrid.h(), mGrid.x(idx), x);
}

int Curve::size() const
{
	return mValues.size();
}
