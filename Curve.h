#ifndef CURVE_H_
#define CURVE_H_

#include "Grid.h"
#include "Vector.h"

class Curve
{
public:
	Curve(double leftBoundary, double rightBoundary, int numPoints);
	Curve(double leftBoundary, double rightBoundary, Vector values);
	void setValues(const Vector& values);
	void setValueAt(int idx, double value);
	Vector& getValues();
	std::vector<double> getDomain() const;
	std::vector<std::pair<double, double>> getPointsInCurve() const;
	double operator()(double x) const;
	int size() const;

private:
	Grid mGrid;
	Vector mValues;
};

#endif // !CURVE_H_
