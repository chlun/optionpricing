#include "BlackScholesExample.h"
#include "../Maths/BlackScholes.h"
#include "../Options/CallPayoff.h"
#include "../Options/PutPayoff.h"
#include "../Options/VanillaOption.h"
#include "../Pdes/BlackScholesCallLogPriceBC.h"
#include "../Pdes/BlackScholesPutLogPriceBC.h"
#include "../Pdes/ConstantBC.h"
#include "../Pricers/FdBlackScholesPricer.h"
#include "../Pricers/McEuropeanPricer.h"

#include <iostream>

void runBlackScholesExample()
{
	auto riskFreeRate = 0.1;
	auto volatility = 0.2;
	auto dividend = 0.05;
	auto strike = 95.0;
	auto expiry = 1.0;
	auto numTimeSteps = 100;
    auto numGridPoints = 1000;

	auto callOption = std::shared_ptr<VanillaOption>(new VanillaOption(
		std::make_shared<CallPayoff>(CallPayoff(strike)),
		expiry));

	auto putOption = std::shared_ptr<VanillaOption>(new VanillaOption(
		std::make_shared<PutPayoff>(PutPayoff(strike)),
		expiry));

	FdBlackScholesPricer pricer(callOption, volatility, riskFreeRate, dividend, strike);

	pricer.setNumGridPoints(numGridPoints)
		.setNumTimeSteps(numTimeSteps)
		.setBoundaryConditionLeft(std::make_shared<ConstantBC>(ConstantBC(0.0)))
		.setBoundaryConditionRight(std::make_shared<BlackScholesCallLogPriceBC>(BlackScholesCallLogPriceBC(strike, riskFreeRate, dividend)));

	pricer.compute();
	pricer.writePricesToFile("FdCallPrices.txt");

	FdBlackScholesPricer putPricer(putOption, volatility, riskFreeRate, dividend, strike);

	putPricer.setNumGridPoints(numGridPoints)
		.setNumTimeSteps(numTimeSteps)
		.setBoundaryConditionRight(std::make_shared<ConstantBC>(ConstantBC(0.0)))
		.setBoundaryConditionLeft(std::make_shared<BlackScholesPutLogPriceBC>(BlackScholesPutLogPriceBC(strike, riskFreeRate, dividend)));

	putPricer.compute();
	putPricer.writePricesToFile("FdPutPrices.txt");

	auto minSpot = strike - 20;
	auto maxSpot = strike + 20;
	auto numSamples = 20000;
	auto numGridPointsMc = 100;
	auto useAntithetic = true;

	auto gbm = std::shared_ptr<ProcessBase>(new GeometricBm(riskFreeRate - dividend, volatility));
	
	McEuropeanPricer<BuiltInGaussianGenerator> mcPricer(callOption, gbm, riskFreeRate, minSpot, maxSpot);

	mcPricer.setAntithetic(useAntithetic)
		.setNumTimeSteps(numTimeSteps)
		.setNumGridPoints(numGridPointsMc);
	
	mcPricer.compute(numSamples);
	mcPricer.writePricesToFile("McCallPrice.txt");

	McEuropeanPricer<BuiltInGaussianGenerator> mcPutPricer(putOption, gbm, riskFreeRate, minSpot, maxSpot);
	
	mcPutPricer.setAntithetic(useAntithetic)
		.setNumTimeSteps(numTimeSteps)
		.setNumGridPoints(numGridPointsMc);

	mcPutPricer.compute(numSamples);
	mcPutPricer.writePricesToFile("McPutPrice.txt");
}
