#include "FdSolver.h"
#include "TriDiagonalOperator.h"
#include "ThetaScheme.h"
#include "../Maths/LinearAlgebra/CG.h"
#include "../Maths/LinearAlgebra/LU.h"

#include <algorithm>
#include <functional>

FdSolver::FdSolver(const std::shared_ptr<SecondOrderParabolicPde>& pde,
	double leftEndPoint,
	double rightEndPoint,
	int numGridPoints,
	double maxTime,
	SpaceDiscretisationMethod spaceDiscMethod,
	TimeDiscretisationMethod timeDiscMethod,
	LinearSolver linearSolver)
	:	mPde(pde),
		mCurrentTime(0.0),
		mMaxTime(maxTime),
		mLeftEndPoint(leftEndPoint),
		mRightEndPoint(rightEndPoint),
		mNumGridPoints(numGridPoints),
		mTimeStepSize(0.5 * (rightEndPoint - leftEndPoint) / numGridPoints),
		mNumTimeSteps(ceil(maxTime / mTimeStepSize)),
		mSolution(leftEndPoint, rightEndPoint, numGridPoints, maxTime, mNumTimeSteps + 1),
		mDiscretisation(nullptr),
		mSpaceDiscretisationMethod(spaceDiscMethod),
		mTimeDiscretisationMethod(timeDiscMethod),
		mLinearSolver(linearSolver),	
		mMaxIter(numGridPoints * numGridPoints),
		mTolerance(1e-6),
		mTheta(0.5)		
{
}

FdSolver& FdSolver::setNumTimeSteps(int numTimeSteps)
{
	if (numTimeSteps <= 0)
	{
		throw std::invalid_argument("Number of time steps must be greater than 0.");
	}

	mNumTimeSteps = numTimeSteps;
	mTimeStepSize = mMaxTime / mNumTimeSteps;
	mSolution = Surface(mLeftEndPoint, mRightEndPoint, mNumGridPoints, mMaxTime, mNumTimeSteps + 1);

	return *this;
}

FdSolver& FdSolver::setTimeStepSize(double timeStepSize)
{
	if (timeStepSize <= 0)
	{
		throw std::invalid_argument("Time step size must be greater than 0.");
	}

	mTimeStepSize = timeStepSize;
	mNumTimeSteps = ceil(mMaxTime / mTimeStepSize);
	mSolution = Surface(mLeftEndPoint, mRightEndPoint, mNumGridPoints, mMaxTime, mNumTimeSteps + 1);

	return *this;
}

FdSolver& FdSolver::setTolerance(double tolerance)
{
	if (tolerance <= 0)
	{
		throw std::invalid_argument("Tolerance must be greater than 0.");
	}

    mTolerance = tolerance;
    return *this;
}

FdSolver& FdSolver::setTheta(double theta)
{
	if (theta < 0 || theta > 1)
	{
		throw std::invalid_argument("theta must be in the interval [0, 1].");
	}

    mTheta = theta;
    return *this;
}

FdSolver& FdSolver::setMaxIter(int maxIter)
{
	if (maxIter <= 0)
	{
		throw std::invalid_argument(
			"The maximum number of iterations must be greater than 0.");
	}

	mMaxIter = maxIter;
	return *this;
}

Surface& FdSolver::solve()
{
	if (mDiscretisation == nullptr)
	{
		initialise();
	}

	auto tau = mDiscretisation->timeStepSize();
	Vector rhs(mNumGridPoints);
	std::function<void(TimeDiscretisation&, Vector&, const Vector&, double)> solve;

	switch (mLinearSolver)
	{
	case ConjugateGradient:
		solve = [=](TimeDiscretisation& A, Vector& u, const Vector& rhs, double t) 
		{ 
			CG(mTolerance, mMaxIter, A, rhs, u, t); 
		};
		break;
	case LuDecomp:
		solve = [](TimeDiscretisation& A, Vector& u, const Vector& rhs, double t) 
		{ 
			LU(A, u, rhs, t); 
		};
		break;
	}

	auto values = mSolution.getTimeSliceAt(mCurrentTime).getValues();
	for (auto step = 0; step < mNumTimeSteps; ++step)
	{
		mDiscretisation->setupEquation(values, rhs, mCurrentTime);
		solve(*mDiscretisation, values, rhs, mCurrentTime);
		mCurrentTime = std::min(mMaxTime, mCurrentTime + tau);
		mSolution.setTimeSliceAt(step + 1, values);
	}

	return mSolution;
}

Surface& FdSolver::getSolution()
{
	return mSolution;
}

void FdSolver::initialise()
{
	std::shared_ptr<SpaceDiscretisation> spaceOp;

	switch (mSpaceDiscretisationMethod)
	{
	case TriDiagonal:
		spaceOp = std::shared_ptr<SpaceDiscretisation>(
			new TriDiagonalOperator(mPde, mLeftEndPoint, mRightEndPoint, mNumGridPoints));
		break;
	}

	switch (mTimeDiscretisationMethod)
	{
	case Theta:
		mDiscretisation = std::unique_ptr<TimeDiscretisation>(
			new ThetaScheme(spaceOp, mTimeStepSize, mTheta));
		break;
	}

	mDiscretisation->getInitialData(mSolution.getTimeSliceAt(0).getValues());
}
