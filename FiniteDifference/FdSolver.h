#ifndef FDSOLVER_H_
#define FDSOLVER_H_

#include "../Surface.h"
#include "../Pdes/SecondOrderParabolicPde.h"
#include "../Vector.h"
#include "TimeDiscretisation.h"

#include <memory>

class FdSolver
{
public:
	enum SpaceDiscretisationMethod
	{
		TriDiagonal,
	};

	enum TimeDiscretisationMethod
	{
		Theta,
	};

	enum LinearSolver
	{
		ConjugateGradient,
		LuDecomp,
	};

	FdSolver(const std::shared_ptr<SecondOrderParabolicPde>& pde,
		double leftEndPoint,
		double rightEndPoint,
		int numGridPoints,
		double maxTime,
		SpaceDiscretisationMethod spaceDiscMethod,
		TimeDiscretisationMethod timeDiscMethod,
		LinearSolver linearSolver);
	FdSolver& setNumTimeSteps(int numTimeSteps);
	FdSolver& setTimeStepSize(double timeStepSize);
	FdSolver& setTolerance(double tolerance);
	FdSolver& setTheta(double theta);
	FdSolver& setMaxIter(int maxIter);
	Surface& solve();
	Surface& getSolution();

private:
	void initialise();
	std::shared_ptr<SecondOrderParabolicPde> mPde;
	double mCurrentTime;
	double mMaxTime;
	double mLeftEndPoint;
	double mRightEndPoint;	
	int mNumGridPoints;
	double mTimeStepSize;
	int mNumTimeSteps;
	Surface mSolution;
	std::unique_ptr<TimeDiscretisation> mDiscretisation;
	SpaceDiscretisationMethod mSpaceDiscretisationMethod;
	TimeDiscretisationMethod mTimeDiscretisationMethod;
	LinearSolver mLinearSolver;
	int mMaxIter;
	double mTolerance;
	double mTheta;
};

#endif // !FDSOLVER_H_
