#ifndef IFDMATRIX_H_
#define IFDMATRIX_H_

#include "../Vector.h"

class IFdMatrix
{
public:
	virtual double operator()(int i, int j, double t) const = 0;
	virtual void multiply(const Vector& y, Vector& z, double t) const = 0;
	virtual int size() const = 0;
	virtual ~IFdMatrix() {}
};

#endif // !IFDMATRIX_H_
