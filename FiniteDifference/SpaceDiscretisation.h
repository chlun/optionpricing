#ifndef SPACEDISCRETISATION_H_
#define SPACEDISCRETISATION_H_

#include "IFDMatrix.h"
#include "../Vector.h"

class SpaceDiscretisation : public IFdMatrix
{
public:
	virtual double gridSpacing() const = 0;
	virtual void getInitialData(Vector& u) const = 0;
	virtual Vector getBCVector(double t) const = 0;
	virtual void applyBC(Vector& u, double t) const = 0;
	virtual ~SpaceDiscretisation() {}
};

#endif // !SPACEDISCRETISATION_H_
