#include "ThetaScheme.h"

ThetaScheme::ThetaScheme(const std::shared_ptr<SpaceDiscretisation>& op, double timeStepSize, double theta)
	: TimeDiscretisation(op, timeStepSize), mTheta(theta)
{
}

ThetaScheme::~ThetaScheme()
{
}

int ThetaScheme::size() const
{
	return mOperator->size();
}

void ThetaScheme::getInitialData(Vector& u) const
{
	mOperator->getInitialData(u);
}

void ThetaScheme::setupEquation(Vector& u, Vector& rhs, double t) const 
{
	mOperator->applyBC(u, t);
	mOperator->multiply(u, rhs, t);
	rhs = u + (rhs * mTimeStepSize * (1 - mTheta));

	// Boundary correction terms.
	rhs = rhs + mOperator->getBCVector(t) * mTimeStepSize * (1 - mTheta)
			  + mOperator->getBCVector(t + mTimeStepSize) * mTimeStepSize * mTheta;

	mOperator->applyBC(u, t + mTimeStepSize);
}

double ThetaScheme::operator()(int i, int j, double t) const
{
	t += mTimeStepSize;

	if (i == j)
	{
		return 1 - mTimeStepSize * mTheta * (*mOperator)(i, j, t);
	}
	
	return -mTimeStepSize * mTheta * (*mOperator)(i, j, t);
}

void ThetaScheme::multiply(const Vector& y, Vector& z, double t) const
{
	mOperator->multiply(y, z, t);
	z = y + (z * -mTimeStepSize * mTheta);
}
