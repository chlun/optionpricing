#ifndef THETASCHEME_H_
#define THETASCHEME_H_

#include "TimeDiscretisation.h"

class ThetaScheme : public TimeDiscretisation
{
public:
	ThetaScheme(const std::shared_ptr<SpaceDiscretisation>& op, double timeStepSize, double theta);
	virtual double operator()(int i, int j, double t) const override;
	virtual void multiply(const Vector& y, Vector& z, double t) const override;
	virtual int size() const override;
	virtual void getInitialData(Vector& u) const override;
	virtual void setupEquation(Vector& u, Vector& rhs, double t) const override;
	virtual ~ThetaScheme();

private:	
	const double mTheta;
};

#endif // !THETASCHEME_H_
