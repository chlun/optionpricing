#ifndef TIMEDISCRETISATION_H_
#define TIMEDISCRETISATION_H_

#include "../FiniteDifference/SpaceDiscretisation.h"

#include <memory>

class TimeDiscretisation : public IFdMatrix
{
public:
	TimeDiscretisation(const std::shared_ptr<SpaceDiscretisation>& op, double timeStepSize) 
		: mOperator(op), mTimeStepSize(timeStepSize) {}
	virtual ~TimeDiscretisation() {}
	virtual void getInitialData(Vector& u) const = 0;
	virtual void setupEquation(Vector& u, Vector& rhs, double t) const = 0;
	virtual double timeStepSize() const { return mTimeStepSize; }

protected:
	std::shared_ptr<SpaceDiscretisation> mOperator;
	const double mTimeStepSize;
};

#endif // !TIMEDISCRETISATION_H_
