#include "TriDiagonalOperator.h"

TriDiagonalOperator::TriDiagonalOperator(const std::shared_ptr<SecondOrderParabolicPde>& pde,
	double leftBoundary,
	double rightBoundary,
	int numGridPoints)
	: mPde(pde), mGrid(Grid(leftBoundary, rightBoundary, numGridPoints))
{	
}

TriDiagonalOperator::~TriDiagonalOperator()
{
}

void TriDiagonalOperator::multiply(const Vector& y, Vector& z, double t) const
{
	auto N = mGrid.N();
	auto h2 = mGrid.h() * mGrid.h();

	for (auto i = 0; i < N; ++i)
	{
		if (i == 0 || i == N - 1)
		{
			z[i] = 0.0;
			continue;
		}

		auto x_i = mGrid.x(i);
		auto yLower = i == 1 ? 0.0 : y[i - 1];
		auto yMid = y[i];
		auto yUpper = i == N - 2 ? 0.0 : y[i + 1];

		z[i] = mPde->diffusion(t, x_i) * (yUpper - 2 * yMid + yLower) / h2
			 + mPde->drift(t, x_i) * (yUpper - yLower) / (2 * mGrid.h())
		     + mPde->discount(t, x_i) * yMid;
	}
}

int TriDiagonalOperator::size() const
{
	return mGrid.N();
}

double TriDiagonalOperator::gridSpacing() const
{
	return mGrid.h();
}

void TriDiagonalOperator::getInitialData(Vector& u) const
{
	for (auto i = 0; i < u.size(); ++i)
	{
		u[i] = mPde->initialCondition(mGrid.x(i));
	}
}

Vector TriDiagonalOperator::getBCVector(double t) const
{
	auto N = mGrid.N();
	auto x_0 = mGrid.x(0);
	auto x_1 = mGrid.x(1);
	auto x_n2 = mGrid.x(N - 2);
	auto x_n1 = mGrid.x(N - 1);
	auto bcLeft = mPde->boundaryConditionLeft(t, x_0);
	auto bcRight = mPde->boundaryConditionRight(t, x_n1);

	Vector v(N);
	v[1] = bcLeft * mPde->diffusion(t, x_1) / (mGrid.h() * mGrid.h())
		 - bcLeft * mPde->drift(t, x_1) / (2 * mGrid.h());

	v[N - 2] = bcRight * mPde->diffusion(t, x_n2) / (mGrid.h() * mGrid.h())
		     + bcRight * mPde->drift(t, x_n2) / (2 * mGrid.h());

	return v;
}

void TriDiagonalOperator::applyBC(Vector& u, double t) const
{
	auto N = mGrid.N();
	u[0] = mPde->boundaryConditionLeft(t, mGrid.x(0));
	u[N - 1] = mPde->boundaryConditionRight(t, mGrid.x(N - 1));
}

double TriDiagonalOperator::operator()(int i, int j, double t) const
{
	auto N = mGrid.N();
	if (i < 0 || j < 0 || i >= N || j >= N)
	{
		throw std::invalid_argument("Index out of range");
	}

	// Non main diagonal terms.
	if (std::fabs(i - j) > 1)
		return 0.0;

	auto x_i = mGrid.x(i);
	auto d = mPde->diffusion(t, x_i) / (mGrid.h() * mGrid.h());
	auto u = mPde->drift(t, x_i) / (2 * mGrid.h());
	auto r = mPde->discount(t, x_i);

	// Upper diagonal terms.
	if (j == i + 1) 
		return d + u;

	// Diagonal terms.
	if (i == j) 
		return -2 * d + r;

	// Lower diagonal terms.
	if (j == i - 1) 
		return d - u;

	return 0.0;
}
