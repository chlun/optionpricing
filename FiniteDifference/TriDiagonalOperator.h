#ifndef TRIDIAGONALOPERATOR_H_
#define TRIDIAGONALOPERATOR_H_

#include "IFdMatrix.h"
#include "SpaceDiscretisation.h"
#include "../Pdes/SecondOrderParabolicPde.h"

#include <memory>

class TriDiagonalOperator : public SpaceDiscretisation
{
public:
	TriDiagonalOperator(const std::shared_ptr<SecondOrderParabolicPde>& pde, 
		double leftBoundary, 
		double rightBoundary,
		int numGridPoints);	
	virtual void multiply(const Vector& y, Vector& z, double t) const override;
	virtual double operator()(int i, int j, double t) const override;
	virtual int size() const override;
	virtual double gridSpacing() const override;
	virtual void getInitialData(Vector& u) const override;
	virtual Vector getBCVector(double t) const override;
	virtual void applyBC(Vector& u, double t) const override;
	virtual ~TriDiagonalOperator() override;

private:
	std::shared_ptr<SecondOrderParabolicPde> mPde;
	const Grid mGrid;
};

#endif // !TRIDIAGONALOPERATOR_H_
