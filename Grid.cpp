#include "Grid.h"

#include <sstream>
#include <stdexcept>

Grid::Grid(double endPointLeft, double endPointRight, int numGridPoints)
	: mEndPointLeft(endPointLeft),
	mEndPointRight(endPointRight),
	mNumGridPoints(numGridPoints),
	mSpacing((endPointRight - endPointLeft) / double(numGridPoints - 1))
{
	if (endPointLeft > endPointRight)
		throw std::invalid_argument("The left boundary cannot be greater than the right boundary.");

	if (numGridPoints < 0)
		throw std::invalid_argument("The number of grid points must be greater than 0.");

	if (numGridPoints == 1 || mEndPointLeft == mEndPointRight)
	{		
		if (numGridPoints != 1 || mEndPointLeft != mEndPointRight)
		{
			std::stringstream ss;
			ss << "Cannot partition interval [" << mEndPointLeft << ", " << mEndPointRight
				<< "] using " << numGridPoints << " grid points.";
			throw std::invalid_argument(ss.str());
		}

		mSpacing = 0.0;
	}
}

Grid::~Grid()
{
}

int Grid::N() const
{
    return mNumGridPoints;
}

double Grid::h() const
{
    return mSpacing;
}

double Grid::x(int i) const
{
	if (i < 0 || i >= mNumGridPoints)
	{
		throw std::invalid_argument("Index must be within the number of grid points");
	}
    return mEndPointLeft + i * mSpacing;
}

int Grid::toIndex(double x) const
{
	if (x < mEndPointLeft || x > mEndPointRight)
	{
		std::stringstream ss;
		ss << "The point x = " << x << " is not within the grid.";
		throw std::out_of_range(ss.str());
	}

	if (mSpacing == 0.0)
	{
		return 0;
	}

	return (int)((x - mEndPointLeft) / mSpacing);
}
