/*
 * Grid.h
 *
 *      Author: chin
 */

#ifndef GRID_H_
#define GRID_H_

class Grid
{
public:
	// Construct grid with specified number of grid points in the specified interval.
    Grid(double endPointLeft, double endPointRight, int numGridPoints);
    ~Grid();

    // Return the number of grid points N.
    int N() const;

    // Return the size of grid spacing h.
    double h() const;

    // Return the i-th grid point.
    double x(int i) const;

    // Return the index of the grid point closest to x.
    int toIndex(double x) const;

private:
    double mEndPointLeft;
    double mEndPointRight;
	int mNumGridPoints;
	double mSpacing;
};


#endif /* GRID_H_ */
