#include "BlackScholes.h"
#include "Random/NormalDistribution.h"

#include <cmath>

double BlackScholesCallPrice(double K, double r, double sigma, double d, double expiry, double t, double S)
{
	auto tau = expiry - t;
	auto d1 = (log(S / K) + (r - d + sigma * sigma / 2.0) * tau) / (sigma * sqrt(tau));
	auto d2 = d1 - sigma * sqrt(tau);
	return S * exp(-d * tau) * StandardNormalCDF(d1) - K * StandardNormalCDF(d2) * exp(-r * tau);
}

double BlackScholesPutPrice(double K, double r, double sigma, double d, double expiry, double t, double S)
{
	auto callPrice = BlackScholesCallPrice(K, r, sigma, d, expiry, t, S);
	auto tau = expiry - t;
	return K * exp(-r * tau) - S * exp(-d * tau) + callPrice;
}
