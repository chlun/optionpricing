#ifndef BLACKSCHOLES_H_
#define BLACKSCHOLES_H_

// Evaluate the Black-Scholes formula for call options.
double BlackScholesCallPrice(double K, double r, double sigma, double d, double expiry, double t, double S);

// Evaluate the Black-Scholes formula for put options.
double BlackScholesPutPrice(double K, double r, double sigma, double d, double expiry, double t, double S);

#endif /* BLACKSCHOLES_H_ */
