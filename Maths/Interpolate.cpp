#include "Interpolate.h"

double linearInterpolate(double leftValue, double rightValue, double gap, double left, double valueAt)
{
	if (gap == 0.0 && leftValue == rightValue)
	{
		return leftValue;
	}

	auto grad = (rightValue - leftValue) / gap;
	return leftValue + grad * (valueAt - left);
}
