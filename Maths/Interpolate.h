#ifndef INTERPOLATE_H_

double linearInterpolate(double leftValue, double rightValue, double gap, double left, double valueAt);

#endif // !INTERPOLATE_H_
