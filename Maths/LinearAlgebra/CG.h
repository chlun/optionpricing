#ifndef CG_H_
#define CG_H_

#include <iostream>

// Implement the CG method for solving systems of linear equations.
template <class Matrix, class Vector>
int CG(double epsilon, int maxIter, const Matrix& A, const Vector& b, Vector& x, double t)
{
	const auto eps2 = epsilon * epsilon * b.norm2();
	// Compute first residual r_0 and the auxiliary variable p_0
	Vector p(b);
	Vector Ap(b);
	A.multiply(x, Ap, t);
	p.axpy(-1, Ap);
	Vector r(p);
	auto error = r.norm2();

	// Start the iteration
	auto iter = 0;
	while (error > eps2 && iter < maxIter)
	{
		// Ap = A*p
		A.multiply(p, Ap, t);
		// alpha = r*r/p*Ap
		auto App = Ap * p;
		auto alpha = error / App;
		// update x and r (x = x+alpha p and r = r-alpha Ap)
		x.axpy(alpha, p);
		r.axpy(-alpha, Ap);
		// compute size of new residual
		auto new_error = r.norm2();
		// update p (p = beta * p + r)
		auto beta = new_error / error;
		p.xpay(beta, r);

		error = new_error;
		++iter;
	}

	if (iter == maxIter)
	{
		std::cout << "CG: iteration aborted since max number of iteration reached!" << std::endl;
	}
	return iter;
}

#endif /* CG_H_ */
