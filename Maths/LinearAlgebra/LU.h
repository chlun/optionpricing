#ifndef LU_H_
#define LU_H_

/*
 *	Solve Ax = b by LU decomposition.
 *	For tridiagonal matrices only.
 *	Assumes the elements of the matrix and vectors are labelled in the
 *	maths convention eg v = (v_1,...v_n).
 */
template <class Matrix, class Vector>
void LU(const Matrix& A, Vector& x, const Vector& b, double t)
{
	auto N = A.size();
	Vector d(N); 	// Diagonal entries of U
	double l;		// Subdiagonal entries of L
	Vector u(N);	// Superdiagonal entries of U
	Vector w(N);    // w = Ux

	d[1] = A(1, 1, t);
	w[1] = b[1];

	for (int i = 2; i < N - 1; ++i)
	{
		l = A(i, i-1, t) / d[i-1];
		u[i-1] = A(i-1, i, t);
		d[i] = A(i, i, t) - l * u[i-1];

		w[i] = b[i] - l * w[i-1];
	}

	x[N-2] = w[N-2] / d[N-2];
	for (auto i = N - 3; i > 0; --i)
	{
		x[i] = (w[i] - u[i] * x[i+1]) / d[i];
	}
}

#endif /* LU_H_ */
