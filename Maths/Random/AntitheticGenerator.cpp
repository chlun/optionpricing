#include "AntitheticGenerator.h"

AntitheticGenerator::AntitheticGenerator(std::unique_ptr<RandomNumberGenerator>& inner)
	: mAntiVariates(inner->getDimension()), mInner(std::move(inner)), mAnti(false)
{
}

void AntitheticGenerator::getRandomNumbers(std::vector<double>& randoms)
{
	if (!mAnti)
	{
		mInner->getRandomNumbers(randoms);
		for (auto i = 0; i < getDimension(); ++i)
		{
			mAntiVariates[i] = -1 * randoms[i];
		}
		mAnti = true;
	}
	else
	{
		randoms = mAntiVariates;
		mAnti = false;
	}
}

void AntitheticGenerator::setDimension(int dimension)
{
	mInner->setDimension(dimension);
	mAntiVariates.resize(dimension);
}

int AntitheticGenerator::getDimension() const
{
	return mInner->getDimension();
}
