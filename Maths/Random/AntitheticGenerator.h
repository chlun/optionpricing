#ifndef ANTITHETICGENERATOR_H_
#define ANTITHETICGENERATOR_H_

#include "RandomNumberGenerator.h"

#include <memory>

// Implements antithetic sampling with Gaussian random variables
class AntitheticGenerator : public RandomNumberGenerator
{
public:
	AntitheticGenerator(std::unique_ptr<RandomNumberGenerator>& inner);
	virtual void getRandomNumbers(std::vector<double>& randoms) override;
	virtual void setDimension(int dimension) override;
	virtual int getDimension() const override;

private:
	std::vector<double> mAntiVariates;	// Store the negative of the previous set of Gaussians.
	std::unique_ptr<RandomNumberGenerator> mInner;	// The method of generating Gaussians.
	bool mAnti;	// Use mAntiVariates if true.
};

#endif // !ANTITHETICGENERATOR_H_
