#include "BuiltInGaussianGenerator.h"

#include <random>

BuiltInGaussianGenerator::BuiltInGaussianGenerator(int dimension)
	: mDimension(dimension)
{
}

// Populate vector with Gaussians
void BuiltInGaussianGenerator::getRandomNumbers(std::vector<double>& randoms)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<> dist(0,1);

	for (auto i = 0; i < mDimension; ++i)
	{
		randoms.push_back(dist(gen));
	}
}

void BuiltInGaussianGenerator::setDimension(int dimension)
{
	mDimension = dimension;
}
