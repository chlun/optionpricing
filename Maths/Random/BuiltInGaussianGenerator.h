#ifndef BUILTINGAUSSIANGENERATOR_H_
#define BUILTINGAUSSIANGENERATOR_H_

#include "RandomNumberGenerator.h"

// Uses the C++ built in Gaussian random number generator
class BuiltInGaussianGenerator : public RandomNumberGenerator
{
public:
	BuiltInGaussianGenerator(int dimension = 1);
	virtual void getRandomNumbers(std::vector<double>& randoms) override;
	virtual void setDimension(int dimension) override;
	virtual int getDimension() const override { return mDimension; }

private:
	int mDimension; // The amount of random numbers to generate
};

#endif // !BUILTINGAUSSIANGENERATOR_H_
