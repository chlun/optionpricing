#define _USE_MATH_DEFINES

#include "NormalDistribution.h"

#include <cmath>

double NormalPDF(double x, double mean, double variance)
{
	return exp(-(x - mean) * (x - mean) / (2 * variance)) / sqrt(2 * M_PI * variance);
}

double StandardNormalCDF(double x)
{
    // Constants
	auto a1 = 0.254829592;
	auto a2 = -0.284496736;
	auto a3 = 1.421413741;
	auto a4 = -1.453152027;
	auto a5 = 1.061405429;
	auto p = 0.3275911;

	// Save the sign of x
	auto sign = x < 0 ? -1 : 1;
	x = fabs(x) / sqrt(2.0);

	auto t = 1.0 / (1.0 + p * x);
	auto y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * exp(-x * x);

	return 0.5 * (1.0 + sign * y);
}
