#ifndef NORMALDISTRIBUTION_H_
#define NORMALDISTRIBUTION_H_

double NormalPDF(double x, double mean = 0.0, double variance = 1.0);
double StandardNormalCDF(double x);

#endif // !NORMALDISTRIBUTION_H_
