#ifndef RANDOMNUMBERGENERATOR_H_
#define RANDOMNUMBERGENERATOR_H_

#include <vector>

class RandomNumberGenerator
{
public:
	RandomNumberGenerator() {}
	virtual void getRandomNumbers(std::vector<double>& randoms) = 0;
	virtual void setDimension(int dimension) = 0;
	virtual int getDimension() const = 0;
	virtual ~RandomNumberGenerator() {}
};

#endif /* RANDOMNUMBERGENERATOR_H_ */
