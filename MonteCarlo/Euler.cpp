#include "Euler.h"

EulerMaruyama::EulerMaruyama(const std::shared_ptr<ProcessBase>& sde,
	int numTimeSteps,
	const std::shared_ptr<RandomNumberGenerator>& rng)
	: mSde(sde), mNumTimeSteps(numTimeSteps), mRng(rng)
{
	if (mNumTimeSteps < 0)
	{
		throw std::invalid_argument("The number of time steps must be greater than 0.");
	}

	// The amount of random numbers needed.
	mRng->setDimension(mNumTimeSteps);
}

double EulerMaruyama::getSolution(double initialValue, double terminalTime) const
{
	if (terminalTime < 0)
	{
		throw std::invalid_argument("Terminal time must be greater than 0.");
	}

	auto timeStepSize = terminalTime / mNumTimeSteps;
	auto rootStepSize = sqrt(timeStepSize);
	auto soln = initialValue;
	auto t = 0.0;
	std::vector<double> randomNumbers;
	mRng->getRandomNumbers(randomNumbers);

	for (auto i = 0; i < mNumTimeSteps; ++i)
	{
		soln += mSde->drift(t, soln) * timeStepSize 
			  + mSde->diffusion(t, soln) * rootStepSize * randomNumbers[i];
		t += timeStepSize;
	}

	return soln;
}
