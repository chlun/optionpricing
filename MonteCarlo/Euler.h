#ifndef EULER_H_
#define EULER_H_

#include "SdeDiscretisation.h"
#include "../StochasticProcesses/ProcessBase.h"
#include "../Maths/Random/RandomNumberGenerator.h"

#include <memory>

class EulerMaruyama : public SdeDiscretisation
{
public:
	EulerMaruyama(const std::shared_ptr<ProcessBase>& sde,
		  int numTimeSteps,
		  const std::shared_ptr<RandomNumberGenerator>& rng);
	virtual double getSolution(double initialValue, double terminalTime) const override;

private:
	std::shared_ptr<ProcessBase> mSde;
	int mNumTimeSteps;
	std::shared_ptr<RandomNumberGenerator> mRng;
};

#endif // !EULER_H_

