#include "McEngine.h"

McEngine::McEngine(const std::shared_ptr<SdeDiscretisation>& sde,
	func toCompute,
	std::unique_ptr<Stats>& statsGatherer)
	:	mSde(sde),
		mToCompute(toCompute),
		mStatsGatherer(std::move(statsGatherer))
{
}

void McEngine::compute(double initialValue, double endTime, int numSamples) const
{
	for (auto i = 0; i < numSamples; ++i)
	{
		mStatsGatherer->dumpOneResult(mToCompute(mSde->getSolution(initialValue, endTime)));
	}
}

std::vector<double> McEngine::getResults() const
{
	return mStatsGatherer->getResults();
}

void McEngine::resetStats()
{
	mStatsGatherer->resetStats();
}
