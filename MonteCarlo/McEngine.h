#ifndef MCENGINE_H_
#define MCENGINE_H_

#include "SdeDiscretisation.h"
#include "Stats/Stats.h"

#include <functional>
#include <memory>

using func = std::function<double(double)>;

class McEngine
{
public:
	McEngine(const std::shared_ptr<SdeDiscretisation>& sde, 
		func toCompute,
		std::unique_ptr<Stats>& statsGatherer);
	void compute(double initialValue, double endTime, int numSamples) const;
	std::vector<double> getResults() const;
	void resetStats();

private:
	std::shared_ptr<SdeDiscretisation> mSde;
	const func mToCompute;
	std::unique_ptr<Stats> mStatsGatherer;
};

#endif // !MCENGINE_H_

