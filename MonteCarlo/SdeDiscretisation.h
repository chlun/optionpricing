#ifndef SDEDISCRETISATION_H_
#define SDEDISCRETISATION_H_

class SdeDiscretisation
{
public:
	SdeDiscretisation() {}
	SdeDiscretisation(const SdeDiscretisation& other) = delete;
	SdeDiscretisation& operator=(SdeDiscretisation& other) = delete;
	virtual ~SdeDiscretisation() {}
	// Get approximation of S_T starting from S_0 = initialValue, T = terminalTime.
	virtual double getSolution(double initialValue, double terminalTime) const = 0;
};

#endif // !SDEDISCRETISATION_H_
