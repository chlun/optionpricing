#include "Moments.h"

Moments::Moments(double k)
	: mExponent(k), mResult(0.0), mNumPathsDone(0)
{
}

void Moments::dumpOneResult(double result)
{
	mResult += pow(result, mExponent);
	mNumPathsDone++;
}

std::vector<double> Moments::getResults() const
{
	std::vector<double> result { mResult / mNumPathsDone };
	return result;
}

void Moments::resetStats()
{
	mResult = 0.0;
	mNumPathsDone = 0;
}
