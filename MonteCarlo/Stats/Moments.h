#ifndef MOMENTS_H_
#define MOMENTS_H_

#include "Stats.h"

// Gathers the k-th moment.
class Moments : public Stats
{
public:
	Moments(double k);
	virtual void dumpOneResult(double result);
	virtual std::vector<double> getResults() const;
	virtual void resetStats();
	virtual ~Moments() {};

private:
	double mExponent;
	double mResult;
	int mNumPathsDone;
};

#endif // !MOMENTS_H_
