#include "MultiStats.h"

MultiStats::MultiStats(const std::vector<Stats*>& stats)
	: mInner(stats)
{
}

void MultiStats::dumpOneResult(double result)
{
	for (unsigned i=0; i<mInner.size(); ++i)
	{
		mInner[i]->dumpOneResult(result);
	}
}

std::vector<double> MultiStats::getResults() const
{
	auto N = mInner.size();
	std::vector<double> results;
	for (unsigned i=0; i<N; ++i)
	{
		auto tmp = mInner[i]->getResults();
		for (unsigned j=0; j<tmp.size(); ++j)
		{
			results.push_back(tmp[j]);
		}
	}
	return results;
}

void MultiStats::resetStats()
{
	for (auto stat : mInner)
	{
		stat->resetStats();
	}
}
