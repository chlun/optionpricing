#ifndef MULTISTATS_H_
#define MULTISTATS_H_

#include "Stats.h"

// Class for gathering multiple statistics.
class MultiStats : public Stats
{
public:
	MultiStats(const std::vector<Stats*>& stats);
	virtual void dumpOneResult(double sample);
	virtual std::vector<double> getResults() const;
	virtual void resetStats();
	virtual ~MultiStats() {};

private:
	std::vector<Stats*> mInner;
};

#endif // !MULTISTATS_H_
