#ifndef STATS_H_
#define STATS_H_

#include <vector>

// Base class for the gathering of different statistics.
class Stats
{
public:
	Stats() {};
	virtual void dumpOneResult(double sample) = 0;
	virtual std::vector<double> getResults() const = 0;
	virtual void resetStats() = 0;
	Stats(const Stats& other) = delete;
	Stats& operator=(Stats &other) = delete;
	virtual ~Stats() {};
};

#endif
