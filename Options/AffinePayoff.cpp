#include "AffinePayoff.h"


AffinePayoff::AffinePayoff(double coeff, double constant)
	: mCoeff(coeff), mConstant(constant)
{
}

double AffinePayoff::operator()(double spot) const
{
	return mCoeff * spot + mConstant;
}
