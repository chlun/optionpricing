#ifndef AFFINEPAYOFF_H_
#define AFFINEPAYOFF_H_

#include "PayOff.h"

class AffinePayoff : public Payoff
{
public:
	AffinePayoff(double coeff, double constant);
	virtual double operator()(double spot) const;

private:
	double mCoeff;
	double mConstant;
};

#endif // !AFFINEPAYOFF_H_
