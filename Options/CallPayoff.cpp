#include "CallPayoff.h"

#include <algorithm>

CallPayoff::CallPayoff(double strike)
	: mStrike(strike)
{
}

double CallPayoff::operator()(double spot) const
{
	return std::max(spot - mStrike, 0.0);
}
