#ifndef CALLPAYOFF_H_
#define CALLPAYOFF_H_

#include "PayOff.h"

class CallPayoff : public Payoff
{
public:
	CallPayoff(double strike);
	virtual double operator()(double spot) const;

private:
	double mStrike;
};

#endif // !CALLPAYOFF_H_
