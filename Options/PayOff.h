#ifndef PAYOFF_H_
#define PAYOFF_H_

class Payoff
{
public:
	Payoff() {};
	virtual double operator()(double spot) const = 0;
	virtual ~Payoff() {}
};

#endif // !PAYOFF_H_

