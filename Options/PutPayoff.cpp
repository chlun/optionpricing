#include "PutPayoff.h"

#include <algorithm>

PutPayoff::PutPayoff(double strike)
	: mStrike(strike)
{
}

double PutPayoff::operator()(double spot) const
{
	return std::max(mStrike - spot, 0.0);
}
