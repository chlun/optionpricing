#ifndef PUTPAYOFF_H_
#define PUTPAYOFF_H_

#include "PayOff.h"

class PutPayoff : public Payoff
{
public:
	PutPayoff(double strike);
	virtual double operator()(double spot) const override;

private:
	double mStrike;
};

#endif // !PUTPAYOFF_H_
