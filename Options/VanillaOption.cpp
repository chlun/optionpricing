#include "VanillaOption.h"

VanillaOption::VanillaOption(const std::shared_ptr<Payoff>& payoff, double expiry)
	: mPayoff(payoff), mExpiry(expiry)
{
}

double VanillaOption::getExpiry() const
{
	return mExpiry;
}

double VanillaOption::payoff(double spot) const
{
	return mPayoff->operator()(spot);
}
