#ifndef VANILLAOPTION_H_
#define VANILLAOPTION_H_

#include "PayOff.h"

#include <memory>

class VanillaOption
{
public:
	VanillaOption(const std::shared_ptr<Payoff>& payoff, double expiry);
	double getExpiry() const;
	double payoff(double spot) const;

private:
	const std::shared_ptr<Payoff> mPayoff;
	double mExpiry;
};

#endif // !VANILLAOPTION_H_
