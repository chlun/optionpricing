#include "BlackScholesCallBC.h"

#include <cmath>

BlackScholesCallBC::BlackScholesCallBC(double strike, double riskFreeRate, double dividend)
	: mStrike(strike), mRiskFreeRate(riskFreeRate), mDividend(dividend)
{
}

double BlackScholesCallBC::operator()(double t, double x) const
{
	return x * exp(-mDividend * t) - mStrike * exp(-mRiskFreeRate * t);
}
