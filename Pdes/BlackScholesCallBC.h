#ifndef BLACKSCHOLESCALLBC_H_
#define BLACKSCHOLESCALLBC_H_

#include "BoundaryCondition.h"

class BlackScholesCallBC : public BoundaryCondition
{
public:
	BlackScholesCallBC(double strike, double riskFreeRate, double dividend);
	virtual double operator()(double t, double x) const override;

private:
	double mStrike;
	double mRiskFreeRate;
	double mDividend;
};

#endif // !BLACKSCHOLESBC_H_
