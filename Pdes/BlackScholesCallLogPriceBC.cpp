#include "BlackScholesCallLogPriceBC.h"

#include <cmath>

BlackScholesCallLogPriceBC::BlackScholesCallLogPriceBC(double strike, double riskFreeRate, double dividend)
	: BlackScholesCallBC(strike, riskFreeRate, dividend)
{
}

double BlackScholesCallLogPriceBC::operator()(double t, double x) const
{
	return BlackScholesCallBC::operator()(t, exp(x));
}
