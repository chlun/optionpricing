#ifndef BLACKSCHOLESCALLLOGPRICEBC_H_
#define BLACKSCHOLESCALLLOGPRICEBC_H_

#include "BlackScholesCallBC.h"

class BlackScholesCallLogPriceBC : public BlackScholesCallBC
{
public:
	BlackScholesCallLogPriceBC(double strike, double riskFreeRate, double dividend);
	virtual double operator()(double t, double x) const override;
};

#endif // !BLACKSCHOLESLOGPRICEBC_H_
