#include "BlackScholesPutBC.h"

#include <cmath>

BlackScholesPutBC::BlackScholesPutBC(double strike, double riskFreeRate, double dividend)
	: mStrike(strike), mRiskFreeRate(riskFreeRate), mDividend(dividend)
{
}

double BlackScholesPutBC::operator()(double t, double x) const
{
	return mStrike * exp(-mRiskFreeRate * t) - x * exp(-mDividend * t);
}
