#ifndef BLACKSCHOLESPUTBC_H_
#define BLACKSCHOLESPUTBC_H_

#include "BoundaryCondition.h"

class BlackScholesPutBC : public BoundaryCondition
{
public:
	BlackScholesPutBC(double strike, double riskFreeRate, double dividend);
	virtual double operator()(double t, double x) const override;

private:
	double mStrike;
	double mRiskFreeRate;
	double mDividend;
};

#endif // !BLACKSCHOLESPUTBC_H_
