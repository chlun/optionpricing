#include "BlackScholesPutLogPriceBC.h"

#include <cmath>

BlackScholesPutLogPriceBC::BlackScholesPutLogPriceBC(double strike, double riskFreeRate, double dividend)
	: BlackScholesPutBC(strike, riskFreeRate, dividend)
{
}

double BlackScholesPutLogPriceBC::operator()(double t, double x) const
{
	return BlackScholesPutBC::operator()(t, exp(x));
}
