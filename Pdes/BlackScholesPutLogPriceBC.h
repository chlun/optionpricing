#ifndef BLACKSCHOLESPUTLOGPRICEBC_H_
#define BLACKSCHOLESPUTLOGPRICEBC_H_

#include "BlackScholesPutBC.h"

class BlackScholesPutLogPriceBC : public BlackScholesPutBC
{
public:
	BlackScholesPutLogPriceBC(double strike, double riskFreeRate, double dividend);
	virtual double operator()(double t, double x) const override;
};

#endif // !BLACKSCHOLESPUTLOGPRICEBC_H_
