#ifndef BOUNDARYCONDITION_H_
#define BOUNDARYCONDITION_H_

class BoundaryCondition
{
public:
	BoundaryCondition() {}
	virtual ~BoundaryCondition() {}
	virtual double operator()(double t, double x) const = 0;
};

#endif // !BOUNDARYCONDITION_H_
