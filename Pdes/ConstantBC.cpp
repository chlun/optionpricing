#include "ConstantBC.h"


ConstantBC::ConstantBC(double constant)
: mConstant(constant)
{
}

double ConstantBC::operator()(double t, double x) const
{
	return mConstant;
}
