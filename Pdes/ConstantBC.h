#ifndef CONSTANTBOUNDARYCONDITION_H_
#define CONSTANTBOUNDARYCONDITION_H_

#include "BoundaryCondition.h"

class ConstantBC : public BoundaryCondition
{
public:
	ConstantBC(double constant);
	virtual ~ConstantBC() {};
	virtual double operator()(double t, double x) const override;

private:
	double mConstant;
};

#endif // !CONSTANTBOUNDARYCONDITION_H_
