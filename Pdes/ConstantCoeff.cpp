#include "ConstantCoeff.h"
#include <cmath>
#include <algorithm>

ConstantCoeff::ConstantCoeff(double drift, 
	double diffusion, 
	double discount,
	func initialCondition,
	const std::shared_ptr<BoundaryCondition>& boundaryConditionLeft,
	const std::shared_ptr<BoundaryCondition>& boundaryConditionRight)
	: SecondOrderParabolicPde(initialCondition, boundaryConditionLeft, boundaryConditionRight),
	  mDrift(drift), 
	  mDiffusion(diffusion), 
	  mDiscount(discount)
{
}

double ConstantCoeff::drift(double t, double x) const
{
	return mDrift;
}

double ConstantCoeff::diffusion(double t, double x) const
{
	return mDiffusion;
}

double ConstantCoeff::discount(double t, double x) const
{
	return mDiscount;
}

double ConstantCoeff::initialCondition(double x) const
{
	return mInitialCondition(x);
}

double ConstantCoeff::boundaryConditionLeft(double t, double x) const
{
	return (*mBoundaryConditionLeft)(t, x);
}

double ConstantCoeff::boundaryConditionRight(double t, double x) const
{
	return (*mBoundaryConditionRight)(t, x);
}

ConstantCoeff::~ConstantCoeff()
{
}
