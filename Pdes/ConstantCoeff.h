#ifndef CONSTANTCOEFF_H_
#define CONSTANTCOEFF_H_

#include "../Pdes/SecondOrderParabolicPde.h"

class ConstantCoeff : public SecondOrderParabolicPde
{
public:
	ConstantCoeff(double drift,
		double diffusion,
		double discount,
		func initialCondition,
		const std::shared_ptr<BoundaryCondition>& boundaryConditionLeft,
		const std::shared_ptr<BoundaryCondition>& boundaryConditionRight);
	virtual double drift(double t, double x) const override;
	virtual double diffusion(double t, double x) const override;
	virtual double discount(double t, double x) const override;
	virtual double initialCondition(double x) const override;
	virtual double boundaryConditionLeft(double t, double x) const override;
	virtual double boundaryConditionRight(double t, double x) const override;
	virtual ~ConstantCoeff();

private:
	double mDrift;
	double mDiffusion;
	double mDiscount;
};

#endif // !CONSTANTCOEFF_H_
