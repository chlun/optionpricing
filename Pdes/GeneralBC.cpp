#include "GeneralBC.h"

GeneralBC::GeneralBC(func2var func)
	: mFunc(func)
{
}

double GeneralBC::operator()(double t, double x) const
{
	return mFunc(t, x);
}
