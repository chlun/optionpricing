#ifndef GENERALBC_H_
#define GENERALBC_H_

#include "BoundaryCondition.h"

#include <functional>

using func2var = std::function<double(double, double)>;

class GeneralBC : public BoundaryCondition
{
public:
	GeneralBC(func2var func);
	virtual double operator()(double t, double x) const override;

private:
	func2var mFunc;
};

#endif // !GENERALBC_H_
