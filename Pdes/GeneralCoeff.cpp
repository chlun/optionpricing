#include "GeneralCoeff.h"

GeneralCoeff::GeneralCoeff(func2var drift, 
	func2var diffusion,
	func2var discount,
	func initialCondition,
	const std::shared_ptr<BoundaryCondition>& boundaryConditionLeft,
	const std::shared_ptr<BoundaryCondition>& boundaryConditionRight)
	: SecondOrderParabolicPde(initialCondition, boundaryConditionLeft, boundaryConditionRight),
      mDrift(drift),
	  mDiffusion(diffusion), 
      mDiscount(discount)
{
}

double GeneralCoeff::drift(double t, double x) const
{
	return mDrift(t, x);
}

double GeneralCoeff::diffusion(double t, double x) const
{
	return mDiffusion(t, x);
}

double GeneralCoeff::discount(double t, double x) const
{
	return mDiscount(t, x);
}

double GeneralCoeff::initialCondition(double x) const
{
	return mInitialCondition(x);
}

double GeneralCoeff::boundaryConditionLeft(double t, double x) const
{
	return (*mBoundaryConditionLeft)(t, x);
}

double GeneralCoeff::boundaryConditionRight(double t, double x) const
{
	return (*mBoundaryConditionRight)(t, x);
}
