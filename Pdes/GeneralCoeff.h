#ifndef GENERALCOEFF_H_
#define GENERALCOEFF_H_

#include "SecondOrderParabolicPde.h"

using func2var = std::function<double(double, double)>;

class GeneralCoeff : public SecondOrderParabolicPde
{
public:
	GeneralCoeff(func2var drift, 
		func2var diffusion,
		func2var discount,
		func initialCondition,
		const std::shared_ptr<BoundaryCondition>& boundaryConditionLeft,
		const std::shared_ptr<BoundaryCondition>& boundaryConditionRight);
	virtual double drift(double t, double x) const override;
	virtual double diffusion(double t, double x) const override;
	virtual double discount(double t, double x) const override;
	virtual double initialCondition(double x) const override;
	virtual double boundaryConditionLeft(double t, double x) const override;
	virtual double boundaryConditionRight(double t, double x) const override;

private:
	func2var mDrift;
	func2var mDiffusion;
	func2var mDiscount;
};

#endif // !GENERALCOEFF_H_

