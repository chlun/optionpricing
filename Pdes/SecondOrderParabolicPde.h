#ifndef SECONDORDERPARABOLICPDE_H_
#define SECONDORDERPARABOLICPDE_H_

#include "BoundaryCondition.h"

#include <functional>
#include <memory>

using func = std::function<double(double)>;

class SecondOrderParabolicPde
{
public:
	SecondOrderParabolicPde(func initialCondition,
		const std::shared_ptr<BoundaryCondition>& bcLeft,
		const std::shared_ptr<BoundaryCondition>& bcRight)
		: mInitialCondition(initialCondition),
		  mBoundaryConditionLeft(bcLeft),
		  mBoundaryConditionRight(bcRight) {}
	virtual double drift(double t, double x) const = 0;
	virtual double diffusion(double t, double x) const = 0;
	virtual double discount(double t, double x) const = 0;
	virtual double initialCondition(double x) const = 0;
	virtual double boundaryConditionLeft(double t, double x) const = 0;
	virtual double boundaryConditionRight(double t, double x) const = 0;
	virtual ~SecondOrderParabolicPde() {};

protected:
	func mInitialCondition;
	std::shared_ptr<BoundaryCondition> mBoundaryConditionLeft;
	std::shared_ptr<BoundaryCondition> mBoundaryConditionRight;
};

#endif // !SECONDORDERPARABOLICPDE_H_
