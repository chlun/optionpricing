#include "FdBlackScholesPricer.h"
#include "../Pdes/ConstantCoeff.h"
#include "../Utils/FileHelper.h"


FdBlackScholesPricer::FdBlackScholesPricer(const std::shared_ptr<VanillaOption>& option,
	double volatility,
	double riskFreeRate,
	double dividend,
	double leftBoundary,
	double rightBoundary)
	// We work with the log price.
	: FdEuropeanPricer(option, 
		std::make_shared<GeometricBm>(GeometricBm(riskFreeRate - dividend, volatility)), 
		leftBoundary == 0.0 ? log(1e-5) : log(leftBoundary),
		log(rightBoundary))
{
	mRates = [riskFreeRate](double t) { return riskFreeRate; };
}

FdBlackScholesPricer::FdBlackScholesPricer(const std::shared_ptr<VanillaOption>& option, 
	double volatility,
	double riskFreeRate, 
	double dividend,
	double strike)
	// We work with the log price.
	: FdEuropeanPricer(option, 
		std::make_shared<GeometricBm>(GeometricBm(riskFreeRate - dividend, volatility)), 
		log(strike) - 1.5,
		log(strike) + 1.5)
{
	mRates = [riskFreeRate](double t) { return riskFreeRate; };
}

void FdBlackScholesPricer::writePricesToFile(double t, std::string filename, std::string delimiter) const
{
	auto curve = mSolver->getSolution().getTimeSliceAt(mOption->getExpiry() - t).getPointsInCurve();
	for (auto& point : curve)
	{
		point.first = exp(point.first);
	}
	
	writeToFile(curve, filename, delimiter);
}

void FdBlackScholesPricer::writePricesToFile(std::string filename, std::string delimiter) const
{
	auto surface = mSolver->getSolution().getAllPoints();
	auto expiry = mOption->getExpiry();

	for (auto& line : surface)
	{
		line.first = expiry - line.first;
		for (auto& point : line.second)
		{
			point.first = exp(point.first);
		}
	}
	
	writeToFile(surface, filename, delimiter);
}

void FdBlackScholesPricer::initialise()
{
	auto sigma = mSde->diffusion(0.0, 1.0);
	auto diffusion = sigma * sigma / 2.0;
	auto drift = mSde->drift(0.0, 1.0) - diffusion;
	auto discount = -mRates(0.0);

	auto pde = std::shared_ptr<SecondOrderParabolicPde>(new ConstantCoeff(
		drift,
		diffusion,
		discount,
		[this](double x) { return mOption->payoff(exp(x)); },
		mBoundaryConditionLeft,
		mBoundaryConditionRight));

	mSolver = std::unique_ptr<FdSolver>(new FdSolver(
		pde,
		mLeftBoundary,
		mRightBoundary,
		mNumGridPoints,
		mOption->getExpiry(),
		FdSolver::SpaceDiscretisationMethod::TriDiagonal,
		FdSolver::TimeDiscretisationMethod::Theta,
		mLinearSolverMethod));
}
