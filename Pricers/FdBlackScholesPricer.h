#ifndef BLACKSCHOLESPRICER_H_
#define BLACKSCHOLESPRICER_H_

#include "FdEuropeanPricer.h"
#include "../StochasticProcesses/GeometricBm.h"

class FdBlackScholesPricer : public FdEuropeanPricer
{
public:
	FdBlackScholesPricer(const std::shared_ptr<VanillaOption>& option,
		double volatility,
		double riskFreeRate,
		double dividend,						
		double leftBoundary,
		double rightBoundary);
	FdBlackScholesPricer(const std::shared_ptr<VanillaOption>& option,
		double volatility,
		double riskFreeRate,
		double dividend,
		double strike);
	virtual void writePricesToFile(double t, std::string filename, std::string delimiter = "\t") const override;
	virtual void writePricesToFile(std::string filename, std::string delimiter = "\t") const override;

private:
	virtual void initialise() override;
};

#endif // !BLACKSCHOLESPRICER_H_
