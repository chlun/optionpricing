#include "FdEuropeanPricer.h"
#include "../Pdes/ConstantBC.h"
#include "../Pdes/GeneralCoeff.h"
#include "../Utils/FileHelper.h"

FdEuropeanPricer::FdEuropeanPricer(const std::shared_ptr<VanillaOption>& option,
	const std::shared_ptr<ProcessBase>& sde,
	double leftBoundary,
	double rightBoundary)
	: mOption(option),
	mSde(sde),
	mSolver(nullptr),
	mLeftBoundary(leftBoundary),
	mRightBoundary(rightBoundary),
	mNumGridPoints(1000),
	mNumTimeSteps(0),
	mRates([](double t) -> double { return 0.0; }),
	mBoundaryConditionLeft(std::make_shared<ConstantBC>(ConstantBC(0.0))),
	mBoundaryConditionRight(std::make_shared<ConstantBC>(ConstantBC(0.0))),
	mTheta(0.5),
	mLinearSolverMethod(FdSolver::LuDecomp)
{
}

FdEuropeanPricer& FdEuropeanPricer::setNumGridPoints(int numGridPoints)
{
	mNumGridPoints = numGridPoints;
	return *this;
}

FdEuropeanPricer& FdEuropeanPricer::setNumTimeSteps(int numTimeSteps)
{
	mNumTimeSteps = numTimeSteps;
	return *this;
}

FdEuropeanPricer& FdEuropeanPricer::setTheta(double theta)
{
	mTheta = theta;
	return *this;
}

FdEuropeanPricer & FdEuropeanPricer::setLinearSolverMethod(FdSolver::LinearSolver method)
{
	mLinearSolverMethod = method;
	return *this;
}

FdEuropeanPricer& FdEuropeanPricer::setRatesFunction(func rates)
{
	mRates = rates;
	return *this;
}

FdEuropeanPricer& FdEuropeanPricer::setBoundaryConditionLeft(std::shared_ptr<BoundaryCondition> bcLeft)
{
	mBoundaryConditionLeft = bcLeft;
	return *this;
}

FdEuropeanPricer& FdEuropeanPricer::setBoundaryConditionRight(std::shared_ptr<BoundaryCondition> bcRight)
{
	mBoundaryConditionRight = bcRight;
	return *this;
}

void FdEuropeanPricer::compute()
{
	if (mSolver == nullptr)
	{
		initialise();
		applyConfigs();
	}

	mSolver->solve();
}

double FdEuropeanPricer::getPriceAt(double t, double spot) const
{
	return mSolver->getSolution()(mOption->getExpiry() - t, spot);
}

Curve& FdEuropeanPricer::getPriceCurve(double t)
{
	return mSolver->getSolution().getTimeSliceAt(mOption->getExpiry() - t);
}

Surface& FdEuropeanPricer::getPriceSurface()
{
	return mSolver->getSolution();
}

void FdEuropeanPricer::writePricesToFile(double t, std::string filename, std::string delimiter) const
{
	const auto& curve = mSolver->getSolution().getTimeSliceAt(mOption->getExpiry() - t);
	writeToFile(curve, filename, delimiter);
}

void FdEuropeanPricer::writePricesToFile(std::string filename, std::string delimiter) const
{
	auto surface = mSolver->getSolution().getAllPoints();
	auto expiry = mOption->getExpiry();

	for (auto& line : surface)
	{
		line.first = expiry - line.first;
	}
	
	writeToFile(surface, filename, delimiter);
}

void FdEuropeanPricer::initialise()
{
	auto pde = std::shared_ptr<SecondOrderParabolicPde>(new GeneralCoeff(
		[this](double t, double x) { return mSde->drift(t, x); },
		[this](double t, double x) { return mSde->diffusion(t, x) * mSde->diffusion(t,x) * 0.5; },
		[this](double t, double x) { return mRates(t); },
		[this](double x) { return mOption->payoff(x); },
		mBoundaryConditionLeft,
		mBoundaryConditionRight));

	mSolver = std::unique_ptr<FdSolver>(new FdSolver(
		pde,
		mLeftBoundary,
		mRightBoundary,
		mNumGridPoints,
		mOption->getExpiry(),
		FdSolver::SpaceDiscretisationMethod::TriDiagonal,
		FdSolver::TimeDiscretisationMethod::Theta,
		mLinearSolverMethod));
}

void FdEuropeanPricer::applyConfigs()
{
	mSolver->setTheta(mTheta);
	if (mNumTimeSteps > 0)
	{
		mSolver->setNumTimeSteps(mNumTimeSteps);
	}
}
