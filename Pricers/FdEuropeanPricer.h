#ifndef FDEUROPEANPRICER_H_
#define FDEUROPEANPRICER_H_

#include "../FiniteDifference/FdSolver.h"
#include "../StochasticProcesses/ProcessBase.h"
#include "../Options/VanillaOption.h"
#include "../Vector.h"

#include <functional>
#include <memory>

using func = std::function<double(double)>;

class FdEuropeanPricer
{
public:
	FdEuropeanPricer(const std::shared_ptr<VanillaOption>& option,
		const std::shared_ptr<ProcessBase>& sde,
		double leftBoundary,
		double rightBoundary);
	virtual ~FdEuropeanPricer() {}
	FdEuropeanPricer& setNumGridPoints(int numGridPoints);
	FdEuropeanPricer& setNumTimeSteps(int numTimeSteps);
	FdEuropeanPricer& setTheta(double theta);
	FdEuropeanPricer& setLinearSolverMethod(FdSolver::LinearSolver method);
	FdEuropeanPricer& setRatesFunction(func rates);
	FdEuropeanPricer& setBoundaryConditionLeft(std::shared_ptr<BoundaryCondition> bcLeft);
	FdEuropeanPricer& setBoundaryConditionRight(std::shared_ptr<BoundaryCondition> bcRight);
	void compute();
	double getPriceAt(double t, double spot) const;
	Curve& getPriceCurve(double t);
	Surface& getPriceSurface();
	virtual void writePricesToFile(double t, std::string filename, std::string delimiter = "\t") const;
	virtual void writePricesToFile(std::string filename, std::string delimiter = "\t") const;

protected:
	virtual void initialise();
	void applyConfigs();
	std::shared_ptr<VanillaOption> mOption;
	std::shared_ptr<ProcessBase> mSde;
	std::unique_ptr<FdSolver> mSolver;
	double mLeftBoundary;
	double mRightBoundary;
	int mNumGridPoints;	
	int mNumTimeSteps;
	func mRates;
	std::shared_ptr<BoundaryCondition> mBoundaryConditionLeft;
	std::shared_ptr<BoundaryCondition> mBoundaryConditionRight;
	double mTheta;
	FdSolver::LinearSolver mLinearSolverMethod;
};

#endif // !FDEUROPEANPRICER_H_
