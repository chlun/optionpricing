#ifndef MCEUROPEANPRICER_H_
#define MCEUROPEANPRICER_H_

#include "../Options/VanillaOption.h"
#include "../StochasticProcesses/ProcessBase.h"
#include "../MonteCarlo/Euler.h"
#include "../MonteCarlo/McEngine.h"
#include "../MonteCarlo/Stats/Moments.h"
#include "../Maths/Random/AntitheticGenerator.h"
#include "../Maths/Random/BuiltInGaussianGenerator.h"
#include "../Utils/FileHelper.h"

#include <memory>
#include <vector>

template <class Rng>
class McEuropeanPricer
{
public:
	McEuropeanPricer(const std::shared_ptr<VanillaOption>& option,
		const std::shared_ptr<ProcessBase>& sde,
		double riskFreeRate,
		double minSpot,
		double maxSpot);
	McEuropeanPricer<Rng>& setNumTimeSteps(int numTimeSteps);
	McEuropeanPricer<Rng>& setNumGridPoints(int numGridPoints);
	McEuropeanPricer<Rng>& setAntithetic(bool enable);
	void compute(int numSamples);
	Curve& getPriceCurve();
	double getPriceAt(double spot) const;
	void writePricesToFile(std::string filename, std::string delimiter = "\t") const;

private:
	void initialise();
	double compute(double spot, int numSamples);
	std::shared_ptr<VanillaOption> mOption;
	std::shared_ptr<ProcessBase> mSde;
	std::unique_ptr<McEngine> mEngine;
	double mRiskFreeRate;
	int mNumTimeSteps;
	bool mAntithetic;
	double mMinSpot;
	double mMaxSpot;
	int mNumGridPoints;
	Curve mSolution;
};


template<class Rng>
McEuropeanPricer<Rng>::McEuropeanPricer(const std::shared_ptr<VanillaOption>& option,
	const std::shared_ptr<ProcessBase>& sde,
	double riskFreeRate,
	double minSpot,
	double maxSpot)
	:   mOption(option),
		mSde(sde),
		mEngine(nullptr),
		mRiskFreeRate(riskFreeRate),
		mNumTimeSteps(100),
		mAntithetic(false),
		mMinSpot(minSpot),
		mMaxSpot(maxSpot),
		mNumGridPoints(minSpot == maxSpot ? 1 : (maxSpot - minSpot) / 0.1),
		mSolution(minSpot, maxSpot, mNumGridPoints)
{
}

template <class Rng>
McEuropeanPricer<Rng>& McEuropeanPricer<Rng>::setNumTimeSteps(int numTimeSteps)
{
	if (numTimeSteps < 0)
	{
		throw std::invalid_argument("The number of time steps must be greater than 0.");
	}

	mNumTimeSteps = numTimeSteps;
	return *this;
}

template<class Rng>
inline McEuropeanPricer<Rng>& McEuropeanPricer<Rng>::setNumGridPoints(int numGridPoints)
{
	mNumGridPoints = numGridPoints;
	mSolution = Curve(mMinSpot, mMaxSpot, mNumGridPoints);
	return *this;
}

template <class Rng>
McEuropeanPricer<Rng>& McEuropeanPricer<Rng>::setAntithetic(bool enable)
{
	mAntithetic = enable;
	return *this;
}

template <class Rng>
inline void McEuropeanPricer<Rng>::compute(int numSamples)
{
	auto idx = 0;
	for (auto point : mSolution.getDomain())
	{
		auto price = compute(point, numSamples);
		mSolution.setValueAt(idx++, price);
	}
}

template <class Rng>
inline Curve& McEuropeanPricer<Rng>::getPriceCurve()
{
	return mSolution;
}

template<class Rng>
inline double McEuropeanPricer<Rng>::getPriceAt(double spot) const
{
	return mSolution(spot);
}

template<class Rng>
inline void McEuropeanPricer<Rng>::writePricesToFile(std::string filename, std::string delimiter) const
{
	writeToFile(mSolution, filename, delimiter);
}

template <class Rng>
double McEuropeanPricer<Rng>::compute(double spot, int numSamples)
{
	if (mEngine == nullptr)
		initialise();

	auto expiry = mOption->getExpiry();
	mEngine->resetStats();
	mEngine->compute(spot, expiry, numSamples);
	return mEngine->getResults()[0] * exp(-mRiskFreeRate * expiry);
}

template <class Rng>
void McEuropeanPricer<Rng>::initialise()
{
	std::shared_ptr<RandomNumberGenerator> rng;
	if (mAntithetic)
	{
		rng = std::shared_ptr<RandomNumberGenerator>(
			new AntitheticGenerator(
				std::unique_ptr<RandomNumberGenerator>(new Rng())));
	}
	else
	{
		rng = std::shared_ptr<RandomNumberGenerator>(new Rng());
	}

	auto solver = std::shared_ptr<SdeDiscretisation>(new EulerMaruyama(mSde, mNumTimeSteps, rng));
	auto mean = std::unique_ptr<Stats>(new Moments(1.0));

	mEngine = std::unique_ptr<McEngine>(new McEngine(solver,
		[this](double spot) -> double { return mOption->payoff(spot); },
		mean));
}

#endif // !MCEUROPEANPRICER_H_
