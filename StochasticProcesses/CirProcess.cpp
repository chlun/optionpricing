#include "CirProcess.h"

#include <cmath>

CirProcess::CirProcess(double kappa, double mu, double sigma)
	: mKappa(kappa), mMu(mu), mSigma(sigma)
{
}

double CirProcess::drift(double t, double x) const
{
	return mKappa * (mMu - x);
}

double CirProcess::diffusion(double t, double x) const
{
	return mSigma * sqrt(x);
}
