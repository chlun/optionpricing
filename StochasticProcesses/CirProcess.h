#ifndef CIRPROCESS_H_
#define CIRPROCESS_H_

#include "ProcessBase.h"

class CirProcess : public ProcessBase
{
public:
	CirProcess(double kappa, double mu, double sigma);
	virtual ~CirProcess() {}
	virtual double drift(double t, double x) const override;
	virtual double diffusion(double t, double x) const override;

private:
	double mKappa;
	double mMu;
	double mSigma;
};

#endif // !CIRPROCESS_H_
