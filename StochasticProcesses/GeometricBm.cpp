#include "GeometricBm.h"

GeometricBm::GeometricBm(double drift, double diffusion)
	: mDrift(drift), mDiffusion(diffusion)
{
}

double GeometricBm::drift(double t, double x) const
{
	return mDrift * x;
}

double GeometricBm::diffusion(double t, double x) const
{
	return mDiffusion * x;
}
