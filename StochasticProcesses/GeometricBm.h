#ifndef GEOMETRICBM_H_
#define GEOMETRICBM_H_

#include "ProcessBase.h"

class GeometricBm : public ProcessBase
{
public:
	GeometricBm(double drift, double diffusion);
	virtual double drift(double t, double x) const override;
	virtual double diffusion(double t, double x) const override;

private:
	double mDrift;
	double mDiffusion;
};

#endif // !GEOMETRICBM_H_

