#ifndef PROCESSBASE_H_
#define PROCESSBASE_H_

class ProcessBase
{
public:
	ProcessBase() {}
	virtual double drift(double t, double x) const = 0;
	virtual double diffusion(double t, double x) const = 0;
	~ProcessBase() {}
};

#endif // !PROCESSBASE_H_
