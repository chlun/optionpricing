#include "Surface.h"
#include "Maths/Interpolate.h"

Surface::Surface(double leftSpaceBoundary, double rightSpaceBoundary, int numSpaceGridPoints, 
	double maxTime, int numTimeSteps)
	: mTimeGrid(0.0, maxTime, numTimeSteps),
	  mCurves(numTimeSteps, Curve(leftSpaceBoundary, rightSpaceBoundary, numSpaceGridPoints))
{
}

double Surface::operator()(double t, double x) const
{
	auto timeIdx = mTimeGrid.toIndex(t);
	auto timeSlice = mCurves[timeIdx];

	auto leftVal = timeSlice(x);
	auto rightVal = leftVal;

	if (timeIdx < mCurves.size() - 1)
	{
		timeSlice = mCurves[timeIdx + 1];
		rightVal = timeSlice(x);
	}

	return linearInterpolate(leftVal, rightVal, mTimeGrid.h(), mTimeGrid.x(timeIdx), t);
}

Curve& Surface::getTimeSliceAt(double t)
{
	auto timeIdx = mTimeGrid.toIndex(t);
	return mCurves[timeIdx];
}

void Surface::setTimeSliceAt(int timeIdx, const Vector & values)
{
	mCurves[timeIdx].setValues(values);
}

std::vector<std::pair<double, Curve>> Surface::getTimeSlices() const
{
	std::vector<std::pair<double, Curve>> timeSlices;
	for (auto i = 0; i < mCurves.size(); ++i)
	{
		timeSlices.push_back(std::make_pair(mTimeGrid.x(i), mCurves[i]));
	}

	return timeSlices;
}

std::vector<std::pair<double, std::vector<std::pair<double, double>>>> Surface::getAllPoints() const
{
	std::vector<std::pair<double, std::vector<std::pair<double, double>>>> points;
	for (auto i = 0; i < mCurves.size(); ++i)
	{
		points.push_back(std::make_pair(mTimeGrid.x(i), mCurves[i].getPointsInCurve()));
	}

	return points;
}
