#ifndef SURFACE_H_
#define SURFACE_H_

#include "Curve.h"
#include "Grid.h"
#include "Vector.h"

class Surface
{
public:
	Surface(double leftSpaceBoundary, double rightSpaceBoundary, int numSpaceGridPoints,
		double maxTime, int numTimeSteps);
	double operator()(double t, double x) const;
	Curve& getTimeSliceAt(double t);
	void setTimeSliceAt(int timeIdx, const Vector& values);
	std::vector<std::pair<double, Curve>> getTimeSlices() const;
	std::vector<std::pair<double, std::vector<std::pair<double, double>>>> getAllPoints() const;

private:
	Grid mTimeGrid;
	std::vector<Curve> mCurves;
};

#endif // !SURFACE_H_
