#include "BlackScholesTest.h"
#include "../Maths/BlackScholes.h"
#include "../Options/CallPayoff.h"
#include "../Options/PayOff.h"
#include "../Options/PutPayoff.h"
#include "../Options/VanillaOption.h"
#include "../Pdes/BlackScholesCallLogPriceBC.h"
#include "../Pdes/BlackScholesPutLogPriceBC.h"
#include "../Pdes/ConstantBC.h"
#include "../Pricers/FdBlackScholesPricer.h"
#include "../Pricers/McEuropeanPricer.h"
#include "../StochasticProcesses/GeometricBm.h"

#include <iostream>
#include <algorithm>


struct TestData
{
	enum OptionType
	{
		Call,
		Put,
	};

	OptionType type;
	double volatility;
	double riskFreeRate;
	double dividend;
	double strike;
	double expiry;
};

double getExactPrice(TestData data, double time, double spot)
{
	if (data.type == TestData::Call)
	{
		return BlackScholesCallPrice(
			data.strike, data.riskFreeRate, data.volatility, data.dividend, data.expiry, time, spot);
	}

	return BlackScholesPutPrice(
		data.strike, data.riskFreeRate, data.volatility, data.dividend, data.expiry, time, spot);
}

void printTestInfo(TestData data, std::string method)
{
	std::cout << "\n\nTesting " << method << "\n"
		<< "Option type: " << data.type << ", "
		<< "Strike: " << data.strike << ", "
		<< "Expiry: " << data.expiry << ", "
		<< "Volatility: " << data.volatility << ", "
		<< "Risk free rate: " << data.riskFreeRate << ", "
		<< "Dividend: " << data.dividend << "\n\n" << std::endl;
}

void printToleranceExceeded(TestData data, double spot, double computed, double actual)
{
	std::cout << "Tolerance exceeded. "
		<< "Computed: " << computed << ", "
		<< "Actual: " << actual
		<< " at spot " << spot << "\n"
		<< "Option type: " << data.type << ", "
		<< "Strike: " << data.strike << ", "
		<< "Expiry: " << data.expiry << ", "
		<< "Volatility: " << data.volatility << ", "
		<< "Risk free rate: " << data.riskFreeRate << ", "
		<< "Dividend: " << data.dividend << std::endl;
}

void runtFdPricer(TestData data, const std::shared_ptr<VanillaOption>& option)
{
	printTestInfo(data, "finite difference");

	auto tolerance = 1e-3;
	auto numGridPoints = 2000;
	auto numTimeSteps = 200;

	FdBlackScholesPricer fdPricer(option, data.volatility, data.riskFreeRate, data.dividend, data.strike);
	fdPricer.setNumGridPoints(numGridPoints).setNumTimeSteps(numTimeSteps);

	if (data.type == TestData::Call)
	{
		fdPricer.setBoundaryConditionLeft(std::make_shared<ConstantBC>(ConstantBC(0.0)))
			.setBoundaryConditionRight(std::make_shared<BlackScholesCallLogPriceBC>(
				BlackScholesCallLogPriceBC(data.strike, data.riskFreeRate, data.dividend)));
	}
	else
	{
		fdPricer.setBoundaryConditionRight(std::make_shared<ConstantBC>(ConstantBC(0.0)))
			.setBoundaryConditionLeft(std::make_shared<BlackScholesPutLogPriceBC>(
				BlackScholesPutLogPriceBC(data.strike, data.riskFreeRate, data.dividend)));
	}

	fdPricer.compute();
	
	for (auto point : fdPricer.getPriceCurve(0.0).getPointsInCurve())
	{ 
		auto actual = getExactPrice(data, 0.0, exp(point.first));
		auto error = std::fabs(point.second - actual);
		
		if (error > tolerance)
		{ 
			printToleranceExceeded(data, exp(point.first), point.second, actual);
		}
	}
}

void runMcPricer(TestData data, const std::shared_ptr<VanillaOption>& option)
{
	printTestInfo(data, "Monte-Carlo");

	auto tolerance = 1e-2;
	auto spot = data.strike + 10;
	auto numTimeSteps = 100;
	auto numSamples = 1e6;
	auto useAntithetic = true;

	auto gbm = std::make_shared<GeometricBm>(GeometricBm(data.riskFreeRate - data.dividend, data.volatility));

	McEuropeanPricer<BuiltInGaussianGenerator> mcPricer(option, gbm, data.riskFreeRate, spot, spot);

	mcPricer.setAntithetic(useAntithetic)
		.setNumTimeSteps(numTimeSteps);

	mcPricer.compute(numSamples);

	for (auto point : mcPricer.getPriceCurve().getPointsInCurve())
	{
		auto actual = getExactPrice(data, 0.0, point.first);
		auto error = std::fabs(point.second - actual);
		
		if (error > tolerance)
		{ 
			printToleranceExceeded(data, point.first, point.second, actual);
		}
	}
}

void testBlackScholes()
{
	std::vector<TestData> testData = {
		{ TestData::Call, 0.1, 0.0, 0.0, 105.0, 1.0 },
		{ TestData::Put, 0.1, 0.0, 0.0, 105.0, 1.0 },
		{ TestData::Call, 0.3, 0.1, 0.0, 19.0, 0.75 },
		{ TestData::Put, 0.3, 0.1, 0.0, 19.0, 0.75 },
		{ TestData::Call, 0.5, 0.1, 0.1, 65.0, 0.5 },
		{ TestData::Put, 0.5, 0.1, 0.1, 65.0, 0.5 },
		{ TestData::Call, 0.3, 0.4, 0.2, 100.0, 0.25 },
		{ TestData::Put, 0.3, 0.4, 0.2, 100.0, 0.25 },
	};

	for (auto i = 0; i < testData.size(); ++i)
	{
		auto data = testData[i];

		std::shared_ptr<VanillaOption> option;
		if (data.type == TestData::Call)
		{
			option = std::make_shared<VanillaOption>(VanillaOption(
				std::make_shared<CallPayoff>(CallPayoff(data.strike)),
				data.expiry));
		}
		else
		{
			option = std::make_shared<VanillaOption>(VanillaOption(
				std::make_shared<PutPayoff>(PutPayoff(data.strike)),
				data.expiry));
		}

		runMcPricer(data, option);
		runtFdPricer(data, option);	
	}
}
