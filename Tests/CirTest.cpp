#include "CirTest.h"
#include "../Options/AffinePayoff.h"
#include "../Options/VanillaOption.h"
#include "../Pdes/GeneralBC.h"
#include "../Pricers/FdBlackScholesPricer.h"
#include "../Pricers/McEuropeanPricer.h"
#include "../StochasticProcesses/CirProcess.h"

#include <iostream>

struct CirTestData
{
	double kappa;
	double mu;
	double sigma;
	double expiry;
};

double cirExpectedValue(double kappa, double mu, double t, double x)
{
	return mu + (x - mu) * exp(-kappa * t);
}

void printTestInfo(CirTestData data, std::string method)
{
	std::cout << "\n\nTesting " << method << "\n"
		<< "kappa: " << data.kappa << ", "
		<< "mu: " << data.mu << ", "
		<< "sigma: " << data.sigma << ", "
		<< "Expiry: " << data.expiry << "\n\n" << std::endl;
}

void printToleranceExceeded(CirTestData data, double spot, double computed, double actual)
{
	std::cout << "Tolerance exceeded. "
		<< "Computed: " << computed << ", "
		<< "Actual: " << actual
		<< " at spot " << spot << "\n"
		<< "kappa: " << data.kappa << ", "
		<< "mu: " << data.mu << ", "
		<< "sigma: " << data.sigma << ", "
		<< "Expiry: " << data.expiry << std::endl;
}

void testFd(CirTestData data, const std::shared_ptr<VanillaOption>& option)
{
	printTestInfo(data, "finite difference");

	auto tolerance = 1e-2;

	auto numTimeSteps = 2000;
	auto numGridPoints = 2000;
	auto minSpot = 0.0;
	auto maxSpot = 50.0;
	auto theta = 1.0;

	auto cirProcess = std::make_shared<CirProcess>(CirProcess(data.kappa, data.mu, data.sigma));

	FdEuropeanPricer fdPricer(option, cirProcess, minSpot, maxSpot);

	auto boundaryCondition = std::make_shared<GeneralBC>(GeneralBC([data](double t, double x)
	{
		return cirExpectedValue(data.kappa, data.mu, t, x);
	}));

	fdPricer.setNumTimeSteps(numTimeSteps)
		.setNumGridPoints(numGridPoints)
		.setTheta(theta)
		.setLinearSolverMethod(FdSolver::LuDecomp)
		.setBoundaryConditionLeft(boundaryCondition)
		.setBoundaryConditionRight(boundaryCondition);

	fdPricer.compute();

	for (auto point : fdPricer.getPriceCurve(0.0).getPointsInCurve())
	{
		auto actual = cirExpectedValue(data.kappa, data.mu, data.expiry, point.first);
		auto error = std::fabs(point.second - actual);

		if (error > tolerance)
		{
			printToleranceExceeded(data, point.first, point.second, actual);
		}
	}
}

void testMc(CirTestData data, const std::shared_ptr<VanillaOption>& option)
{
	printTestInfo(data, "Monte-Carlo");

	auto tolerance = 1e-2;

	auto numGridPoints = 10;
	auto numTimeSteps = 2000;
	auto minSpot = 0.0;
	auto maxSpot = 50.0;
	auto numSamples = 20000;
	auto useAntithetic = true;

	auto cirProcess = std::make_shared<CirProcess>(CirProcess(data.kappa, data.mu, data.sigma));

	McEuropeanPricer<BuiltInGaussianGenerator> mcPricer(option, cirProcess, 0.0, minSpot, maxSpot);

	mcPricer.setAntithetic(useAntithetic)
		.setNumTimeSteps(numTimeSteps)
		.setNumGridPoints(numGridPoints);

	mcPricer.compute(numSamples);

	for (auto point : mcPricer.getPriceCurve().getPointsInCurve())
	{
		auto actual = cirExpectedValue(data.kappa, data.mu, data.expiry, point.first);
		auto error = std::fabs(point.second - actual);

		if (error > tolerance)
		{
			printToleranceExceeded(data, point.first, point.second, actual);
		}
	}
}

void testCir()
{
	std::vector<CirTestData> testData{
		CirTestData{ 2.0, 0.5, 0.1, 1.0 },
		CirTestData{ 2.0, 0.5, 1.0, 1.0 },
	};

	for (auto data : testData)
	{
		auto option = std::make_shared<VanillaOption>(VanillaOption(
			std::make_shared<AffinePayoff>(AffinePayoff(1.0, 0.0)),
			data.expiry));

		testFd(data, option);
		testMc(data, option);
	}
}
