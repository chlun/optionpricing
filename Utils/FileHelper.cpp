#include "FileHelper.h"

#include <iostream>
#include <fstream>


void writeToFile(const Surface& function, std::string filename, std::string delimiter)
{
	writeToFile(function.getAllPoints(), filename, delimiter);
}

void writeToFile(const std::vector<std::pair<double, std::vector<std::pair<double, double>>>>& surface,
	std::string filename,
	std::string delimiter)
{
	std::ofstream ofs(filename);

	if (!ofs)
	{
		std::cout << "Unable to open file " << filename << " for writing." << std::endl;
		return;
	}

	for (auto line : surface)
	{
		for (auto point : line.second)
		{
			ofs << line.first << delimiter
				<< point.first << delimiter
				<< point.second << "\n";
		}
	}

	std::cout << "Successfully written to " << filename << std::endl;
}

void writeToFile(const Curve& function, std::string filename, std::string delimter)
{
	writeToFile(function.getPointsInCurve(), filename, delimter);
}

void writeToFile(const std::vector<std::pair<double, double>>& function,
	std::string filename,
	std::string delimter)
{
	std::ofstream ofs(filename);

	if (!ofs)
	{
		std::cout << "Unable to open file " << filename << " for writing." << std::endl;
		return;
	}

	for (auto point : function)
	{
		ofs << point.first << delimter << point.second << "\n";
	}

	std::cout << "Successfully written to " << filename << std::endl;
}
