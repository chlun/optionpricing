#ifndef FILEHELPER_H_
#define FILEHELPER_H_

#include "../Surface.h"

#include <string>

void writeToFile(const Surface& function, std::string filename, std::string delimiter = "\t");

void writeToFile(const std::vector<std::pair<double, std::vector<std::pair<double, double>>>>& values,
	std::string filename,
	std::string delimiter = "\t");

void writeToFile(const Curve& function, std::string filename, std::string delimter = "\t");

void writeToFile(const std::vector<std::pair<double, double>>& function, 
	std::string filename,
	std::string delimter = "\t");

#endif // !FILEHELPER_H_
