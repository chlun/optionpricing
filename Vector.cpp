/*
 * Vector.cpp
 *
 *      Author: chin
 */

#include "Vector.h"

Vector::Vector(unsigned int N)
	: mValues(std::vector<double>(N))
{
}

Vector::Vector(unsigned int N, double a)
	: mValues(std::vector<double>(N, a))
{
}

Vector::Vector(const Grid& grid)
	: Vector(grid.N())
{
}

Vector::Vector(const Vector& other)
{
	mValues = other.mValues;
}

double& Vector::operator[](unsigned int i)
{
	return mValues.at(i);
}

const double& Vector::operator[](unsigned int i) const
{
	return mValues.at(i);
}

double Vector::operator*(const Vector& y) const
{
	auto result = 0.0;
	for (auto i = 0; i < mValues.size(); ++i)
	{
		result += (*this)[i] * y[i];
	}
	return result;
}

Vector Vector::operator*(double a) const
{
	Vector v((*this).size());
	for (auto i = 0; i < mValues.size(); ++i)
	{
		v[i] = a * (*this)[i];
	}

	return v;
}

Vector Vector::operator+(const Vector& y) const
{
	Vector v(size());
	for (auto i = 0; i < size(); ++i)
	{
		v[i] = (*this)[i] + y[i];
	}
	
	return v;
}

void Vector::axpy(double a, const Vector& x)
{
	for (auto i = 0; i < mValues.size(); ++i)
	{
		(*this)[i] += a * x[i];
	}
}

void Vector::xpay(double a, const Vector& x)
{
	for (auto i = 0; i < mValues.size(); ++i)
    {
		(*this)[i] = x[i] + a * (*this)[i];
    }
}

double Vector::norm2() const
{
	return (*this) * (*this);
}

int Vector::size() const
{
	return mValues.size();
}
