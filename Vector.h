/*
 * Vector.h
 *
 *      Author: chin
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include "Grid.h"
#include <vector>

class Vector
{
public:
	Vector(unsigned int N);
	Vector(unsigned int N, double a);
	// Construct a vector of size equal to the number of grid points and
	// initialise with the value 0.0.
	Vector(const Grid& grid);
	Vector(const Vector& other);
	double& operator[](unsigned int i);
	const double& operator[](unsigned int i) const;
	// Compute the scalar product of this vector with another vector.
	double operator*(const Vector& y) const;
	// Multiply this vector by a constant.
	Vector operator*(double a) const;
	Vector operator+(const Vector& y) const;
	// this -> a * x + this
	void axpy(double a, const Vector& x);
	// this -> x + a * this
	void xpay(double a, const Vector& x);
	// Compute the squared l2 norm of this vector.
	double norm2() const;
	int size() const;

private:
	std::vector<double> mValues;
};

#endif /* VECTOR_H_ */
