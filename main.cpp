#include "Examples/BlackScholesExample.h"

#include <iostream>
#include <stdexcept>

int main()
{
	try
	{
		runBlackScholesExample();
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << "\nDone" << std::endl;
	system("pause");
}
